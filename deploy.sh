#!/bin/bash
rm -rf /var/www/html/philippe-in-sf.github.io
cd /var/www/html/
git clone https://philippebeaudette@bitbucket.org/philippebeaudette/philippe-in-sf.github.io.git
mv philippe-in-sf.github.io/* /var/www/html
chmod u+x /var/www/html/deploy.sh
rm -rf philippe-in-sf.github.io
